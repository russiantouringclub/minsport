const osmosis = require('osmosis')
const fs = require('fs')

var file = fs.createWriteStream('./output.json')
var savedData = [],
  website = 'https://www.minsport.gov.ru/documents/awards'

file.on('open', function (fd) {
  (async () => {
    await osmosis
          .get(website)
          .find('.item')
          .set({
            'title': 'h4',
            'url': '@href',
            'number': '.text',
            'date': '.date',
            'content': [
              osmosis
              .follow('a @href')
              .find('.detail-text')

            ]
          })
          .then(function (ctx, data, next, done) {
            data.number = data.number.split('\n')[0]
            data.date = data.date.split(': ')[1]
            data.url = 'https://www.minsport.gov.ru' + data.url
            data.content = data.content[0].trim().split('\r\n').map(function (el) {
              return el.trim()
            })
              .filter(function (el) {
                return el != ''
              })
            // console.log(data)
            file.write(Buffer.from(JSON.stringify(data), 'utf8'))

            let s, e, people = [], str = []
            let resStr = `Приказ Минспорта России ${data.content[1]}
            ${data.content[3]} по виду спорта "спортивный туризм"
            `
            let sport = 'Спортивный туризм'
            str[0] = data.content.slice(2).join('\n')
            let searchStart = str[0].indexOf(sport)
            let searchEnd, endText
            str[1] = str[0].slice(searchStart + searchStart.length).split('\n')
            str[1].forEach((el, i, arr) => {
              console.log('LOG', el.slice(0, 3))

              if (el.slice(0, 3).match(/^[А-Я]{3}/)) {
                people.push(el)
              } else endText = el
              if (el.startsWith('Министр')) arr.splice(i, 1)
              if (el.slice(0, 3).match(/^[А-Я]{1}[-z]{1}/)) {
                people.push(el)
              } else endText = el
            })
            searchEnd = str[0].indexOf(endText)
            str[2] = str[0].slice(searchStart + searchStart.length, searchEnd)
            console.log('resStr', resStr, endText)

            savedData.push(data)
          })
          .data((data) => {
            console.log(`data`, this)
            savedData.push(data)
          })
          .done(() => {
            console.log('saved', savedData)
            fs.writeFile('data.json', JSON.stringify(savedData, null, 4), function (err) {
              if (err) console.error(err)
              else console.log('Data Saved to data.json file')
            })
          })
    console.log('savedData', savedData)
    await osmosis.then(function () {
      savedData = savedData.filter((el, i, arr) => {
        return el.content.indexOf('Спортивный туризм') >= 0
      })
    })
  })()
}
)
